<?php

namespace Model\Table;

use Lib\Banco\Banco;

class UsuariosTable
{
    private $banco;
    
    public function __construct($banco)
    {
       $this->banco =  Banco::getInstance();
       
    }
    
    public function listarUsuarios()
    {
        $query = 'SELECT * FROM usuarios';
        $result = $this->banco->query($query);
        return $result->fetchAll(\PDO::FETCH_ASSOC);
    }
    
    public function excluirUsuario($id)
    {
        $query = "DELETE FROM usuarios WHERE id = $id";
        return $this->banco->exec($query);
    }
    
    public function inserirUsuario(string $nome, string $email, string $senha)
    {
        $query = "INSERT INTO usuarios(nome,email,senha)VALUES('$nome','$email','$senha')";
        return $this->banco->exec($query);
    }
    
    public function buscarUsuarioPorId(int $id)
    {
        $query = "SELECT * FROM usuarios WHERE id = $id";
        return $this->banco->query($query)->fetch(\PDO::FETCH_ASSOC);
    }
    
    public function alterarUsuario(int $id, string $nome, string $email, string $senha)
    {
        $query = "UPDATE usuarios SETnome=$nome, email=$email, senha=$senha WHERE id = $id";
        return $this->banco->exec($query);
    }
    
    
}