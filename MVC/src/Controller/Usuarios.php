<?php

namespace Controller;

use Model\Table\UsuariosTable;

class Usuarios
{
    private $table;
    
    public function __construct()
    {
        $this->table = new UsuariosTable();
    }
    public function listar() 
    {
        return $this->table->listarUsuarios();
    }
    
    public function inserir()
    {
        if($_POST){
            $nome = $_POST['nome'];
            $email = $_POST['email'];
            $senha = $_POST['senha'];
            $return = $this->table->inserirUsuario($nome, $email, $senha);
            
            if(! $return){
                throw new \Exception('Erro ao excluir o Usuário');
                exit();
            }
            
            header('location:index.php?rota=usuarios/listar');
        }
    }
    
    public function excluir($id)
    {
        $return = $this->table->excluirUsuario($id);
        
        if(! $return){
            throw new \Exception('Erro ao excluir o Usuário');
            exit();
        }
        
        header('location:index.php?rota=usuarios/listar');
    }
    
    public function alterar($id)
    {
        if($_POST){
            $nome = $_POST['nome'];
            $email = $_POST['email'];
            $senha = $_POST['senha'];
            $return = $this->table->alterarUsuario($id, $nome, $email, $senha);
            
            if(! $return){
                throw new \Exception('Erro ao excluir o Usuário');
                exit();
            }
            
            header('location:index.php?rota=usuarios/listar');
        }
        $usuario = $this->table->buscarUsuarioPorId($id);
        return $usuario;
    }
}