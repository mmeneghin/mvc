<?php

namespace Lib\Controller;
use Lib\View\ViewModel;

class FrontController
{
    public function run() 
    {
        
        $rota = $_GET['rota'] ?? 'index/index';
        $parserRota = explode('/', $rota);
        $controller = $parserRota[0];
        $action     = $parserRota[1];
        $id         = $parserRota[2] ?? null;
        $caminhoView = "$controller/$action";
        $controller = 'Controller\\' . ucfirst($controller);
        $objController = new $controller();
        
        if(! method_exists($objController, $action))
        {
            throw new \Exception('RotaInválida');
            return;
            exit();
        }
        
        $dados = $objController->$action($id);
        
        ViewModel::render($caminhoView, $dados);

    }
}
