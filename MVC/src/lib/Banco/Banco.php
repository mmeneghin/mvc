<?php


namespace Lib\Banco;

class Banco
{
      
    private static $pdo;
    
    public static function getInstance()
    {
        
        $dsn = 'pgsql:host=localhost;dbname=aplicacao';
        $user = 'curso501';
        $pass = 'curso501@secret';
        
        if(! self::$pdo){
            
            self::$pdo = new \PDO($dsn, $user, $pass);
        }
        
        return self::$pdo;
    }
    private function __construct(){
    }
    
    private function __clone(){
    }
    
}